- name: install system packages
  community.general.zypper:
    update_cache: true
    name:
      - python3-psycopg2
      - python3-pyOpenSSL
      - python3-virtualenv
      - gnuhealth-orthanc
      - uwsgi
      - uwsgi-python3
      - postgresql

# add thalamus user with postgresql permissions
- name: add thalamus user
  ansible.builtin.user:
    name: thalamus
    password: "{{ my_thlms_password | password_hash('sha512') }}"
    shell: /bin/bash
- name: start psql
  ansible.builtin.systemd:
    state: started
    name: postgresql
## somehow become is not working under opensuse like it does under ubuntu for postgres and thalamus
## this prevents using "become_user" thus the warning when executing is justified but not avoidable afaik
- name: show roles
  command: su - postgres -c "psql -c '\du'"
  register: roles
  changed_when: false
- name: create psql role
  command: su - postgres -c "psql -c 'CREATE USER thalamus WITH CREATEDB;'"
  when: "'thalamus' not in roles.stdout"
- name: check if database exists
  command: su - postgres -c "psql -lqt | cut -d \| -f 1 | grep -w 'federation'"
  register: search_db
  failed_when: search_db.rc not in [0,1]
  changed_when: false
- name: create postgresql database
  command: su - postgres -c "createdb 'federation' --encoding="UTF-8" --owner=thalamus"
  when: not 'federation' in search_db.stdout
- name: change psql config
  ansible.builtin.lineinfile:
    path: "/var/lib/pgsql/data/pg_hba.conf"
    line: "local\tall\t\tall\t\t\t\t\ttrust"
- name: restart and enable postgresql
  ansible.builtin.systemd:
    state: restarted
    name: postgresql
    enabled: true

- name: install thalamus with pip inside venv
  ansible.builtin.pip:
    virtualenv: /opt/venv
    virtualenv_site_packages: true
    name:
      - wheel
      - "thalamus=={{ thalamus_version }}"
      - flask-cors
- name: get thalamus location
  shell: 'set -o pipefail && /opt/venv/bin/pip3 show thalamus | grep Location'
  args:
    executable: /bin/bash
  register: thlms_location
  changed_when: false
- name: set postgresql connection in import_pg.py
  ansible.builtin.lineinfile:
    path: "{{ thlms_location.stdout[10:] }}/thalamus/demo/import_pg.py"
    line: "    conn = psycopg2.connect(dbname='federation')"
    regexp: "^(.*)conn = psycopg2.connect(.*)$"
- name: set postgresql connection in thalamus.py
  ansible.builtin.lineinfile:
    path: "{{ thlms_location.stdout[10:] }}/thalamus/thalamus.py"
    line: "conn = psycopg2.connect(dbname='federation')"
    regexp: "^(.*)conn = psycopg2.connect(.*)$"
- name: init federation db
  command: su - thalamus -c "psql -d federation < {{ thlms_location.stdout[10:] }}/thalamus/demo/federation_schema.sql"
- name: populate federation db
  command: su - thalamus -c "cd {{ thlms_location.stdout[10:] }}/thalamus/demo/ && bash populate.sh"
  when: populate_db
- name: add lines to etc/thalamus_uwsgi.ini
  ansible.builtin.lineinfile:
    path: "{{ thlms_location.stdout[10:] }}/thalamus/etc/thalamus_uwsgi.ini"
    state: present
    line: "{{ item }}"
  loop:
    - "http = 0.0.0.0:{{ http_port_in }}"
    - "plugins = http,python3"
    - "venv = /opt/venv/"
- name: remove https line from etc/thalamus_uwsgi.ini
  ansible.builtin.lineinfile:
    path: "{{ thlms_location.stdout[10:] }}/thalamus/etc/thalamus_uwsgi.ini"
    state: absent
    regexp: "^(.*)https =(.*)$"
- name: generate random string for secret key in thalamus config
  ansible.builtin.set_fact:
    rand_string: "{{ lookup('password', '/dev/null length=15 chars=ascii_letters') }}"
  no_log: true
- name: set secret key in thalamus config
  ansible.builtin.replace:
    path: "{{ thlms_location.stdout[10:] }}/thalamus/etc/thalamus.cfg"
    regexp: '^(.*)SECRET_KEY(.*)$'
    replace: "SECRET_KEY = '{{ rand_string }}'"
  no_log: true

# systemd service
- name: copy systemd service
  ansible.builtin.copy: src=thalamus/thalamus.service dest=/etc/systemd/system/
- name: set path in systemd service
  ansible.builtin.lineinfile:
    path: "/etc/systemd/system/thalamus.service"
    regexp: "^(.*)WorkingDirectory=(.*)$"
    line: "WorkingDirectory={{ thlms_location.stdout[10:] }}/thalamus"
- name: restart and enable thalamus service
  ansible.builtin.systemd:
    state: restarted
    daemon_reload: true
    enabled: true
    name: thalamus

