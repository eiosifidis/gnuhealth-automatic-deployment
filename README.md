# Automatic Deployment of the open source hospital information system GNU Health using Ansible
The Ansible playbooks in this project aim to automatically deploy the hospital information system GNU Health including the HMIS node, Thalamus, the DICOM server Orthanc and a desktop environment as a client using the GNU Health HMIS node.  
**HMIS node**: Hospital management information system, core of the GNU Health system  
**Thalamus** (GNU Health Federation): Creating a Network to converge multiple HMIS nodes and MyGNUHealth devices  
**Orthanc**: The DICOM server is not directly part of the GNU Health system but its integration is provided  
Supported operating systems are Ubuntu 20.04 LTS, openSUSE Leap 15.3 and Debian 11.  
Find the full documentation here:  
https://geraldwiese.gitlab.io/gnuhealth-automatic-deployment/

# Main functionality
The main steps of the playbooks are the following:
- GNU Health (HMIS node):
    - Follow main installation instructions
    - Install demo database
    - Install WebDAV server
    - Create systemd service(s)
- Thalamus:
    - Follow installation instructions including uWSGI, venv & systemd service
- Orthanc:
    - Follow main installation instructions
    - Configuration: allow HTTP access, disable DICOM access, create users with different permissions
- All servers:
    - Either copy existing certificate and key in the right place or create them (self-signed then)
    - Create HTTPS reverse proxies using Apache or Nginx
    - Automatic backups using cron jobs
    - More security features: fail2ban, automatic upgrades/reboot, monitoring important services and
     e-mail notifications (either provide a "sendmail" command yourself or use ssmtp+dummy mail account)
- Desktop:
    - Install GNU Health Client
    - Install the crypto plugin
    - Create configuration profiles for accessing the deployed server
    - Place profiles and plugins in /etc/skel/ for new users as well
    - Install gpg and create a key in order to use the crypto plugin
    - Install MyGNUHealth (Working on Ubuntu 21.04 and on openSUSE Leap 15.3)  
    
All of the tasks are optional and can be enabled/disabled using booleans in vars files. For all servers the four main steps are controlled by the booleans installation, cert_and_rproxy, security_features & cron_backups in server_subdir/vars. Substeps are also controlled by additional booleans. For example the WebDAV server will only be installed if _installation_ and _webdav_ booleans are true for gnuhealth.  
Note that the integration of Orthanc and the development of Thalamus & MyGNUHealth is still in progress.  

# Requirements
- Install git and clone the repository  
```sudo apt install git```  
```git clone https://gitlab.com/geraldwiese/gnuhealth-automatic-deployment.git```  
- Install ansible  
```sudo apt install ansible```  
and two collections as well:  
```ansible-galaxy collection install community.crypto community.general```  
- In case of remote access:
    - Preferably setup a ssh key or otherwise install sshpass where you call the playbooks:  
    ```sudo apt install sshpass```  
    - Make sure the remote machines are accessible via ssh:  
    ```sudo apt install openssh-server``` if not already installed  
    ```sudo ufw allow ssh``` if the port was closed  
- If you run the playbook on the target system you need neither sshpass nor openssh-server but have to set
'--connection=local' when running the playbook, for example:  
```ansible-playbook gnuhealth.yml --connection=local```
- Hardware: If using virtual machines take 20GB storage and 2GB RAM to be sure. For testing purposes 1GB RAM and 15GB storage should be enough as well (for storage it's no difference if you allocate the storage dynamically anyway).

# Minimal configuration
- Set your IP or domain in hosts under "[gnuhealth]" (not necessary if you run it locally)
- Set at least _vault_ssh_user_ and _vault_ssh_password_ in the variable file gnuhealth/vault (used for ssh, sudo and paths)
- Set _my_domain_ in gnuhealth/vars if remote access is desired (for cert & reverse proxy)
- For every deployment there are variable files vars and vault.  
- Do the same for "thalamus", "orthanc" or "desktop", no need for a domain at "desktop"

## Example 1
GNU Health server and client on one system, running Ansible locally on the same system, generating a self signed certificate
- Clone the repository and navigate inside its top level directory
- Set _vault_ssh_user_ and _vault_ssh_password_ in gnuhealth/vault
- Run the playbook gnuhealth.yml: ```ansible-playbook gnuhealth.yml --connection=local```
- Set _vault_ssh_user_ and _vault_ssh_password_ in desktop/vault
- In desktop/vars set _trust_selfsigned_certs_: true
- Run the playbook desktop.yml: ```ansible-playbook desktop.yml --connection=local```
- Run the client either using the desktop entry or from terminal: ```gnuhealth-client```
- In the client the connection informations are already taken from the previous installation thus just hit _Connect_
- Enter the password (vault_tryton_pw in gnuhealth/vault, 789 if not changed)

## Example 2
GNU Health server, Orthanc server and GNU Health client each on a different system, running Ansible remote, using existent certificate+key
- Clone the repository and find out all domain names
- Set the corresponding domains or IPs under [gnuhealth], [orthanc] and [desktop] in the file _hosts_
- Set vault_ssh_user and vault_ssh_password in gnuhealth/vault
- In gnuhealth/vars set:
    - my_domain
    - create_selfsigned_cert: false
    - remote_source: true if cert+key are already on the target system
    - set paths my_cert and my_key
- ```ansible-playbook gnuhealth.yml```
- Repeat the previous steps for vars+vault in orthanc/ and run the playbook orthanc.yml
- Set user and password for desktop
- Run the playbook desktop.yml and run the client like described above  
If you run Ansible from one of the three systems, install Ansible and sshpass on this one and openssh-server on the others. Using --connection=local when calling the playbook will ignore IP/domain in hosts.

# Certificate
For the servers HTTPS communication certificates are required.
The playbooks will generate a selfsigned certificate itself if the corresponding boolean is true.
Alternatively you can use your own certificate.  
The certificate and its chain are expected to be in one file. If you have your certificate "cert.pem" and its chain "chain.pem" combine it using  
```cp cert.pem cert-chain.pem```  
```cat chain.pem >> cert-chain.pem```  
The paths for the certificate and the key have to be entered in the corresponding vars file.
In case of remote access set "remote_source" true if the certificate and key are already on the server.

# Encryption by Ansible-Vault
Don't store your passwords in plain text. Encrypt the password files:  
```ansible-vault encrypt gnuhealth/vault```  
Afterwards the playbook is executed like this:  
```ansibe-playbook gnuhealth.yml --ask-vault```  
If you access the target machine from remote but have the private key for the certificate on the system running ansible, encrypt it as well:  
```ansible-vault encrypt /path/to/key.pem```  
Having two encrypted files - variables in group_vars/vault and the key - the playbook has to be run in the following way:  
```ansible-playbook gnuhealth.yml --vault-id vault@prompt --vault-id key@prompt```  
For temporary unencrypted access decrypt them:
```ansible-vault decrypt gnuhealth/vault```

# Running a playbook on multiple hosts
If you want to run a playbook on multiple hosts - for example deploying many working stations with the GNU Health client - just add multiple lines of domains or IPs under [desktop] in hosts. It's also possible to use numerical or alphabetical ranges like this:  
>     [desktop]  
>     my_domain[00:20]  
>     another_domain_[a:e]  


# Firewall
The playbooks won't manage your firewall rules. This might close a non-default ssh port or have no effect if running on virtual machines. Make sure that incoming traffic is denied by default and that your internal server ports are closed but ssh and reverse proxies ports are open.  
For example the GNU Health setup using the default ports requires 22, 443 and 4443 to be open if using WebDAV server and the internal ports 8000 and 8080 should be closed.

# Links
Ansible documentation https://docs.ansible.com/  
GNU Health Wikibook https://en.wikibooks.org/wiki/GNU_Health  
Orthanc Book https://book.orthanc-server.com/  
Apache documentation https://httpd.apache.org/docs/current/  
Nginx documentation https://nginx.org/en/docs/  
Fail2Ban https://www.fail2ban.org/  


For informations about details in the deployments, securing SSH & passwords, backups, etc. read the GitLab Pages documentation of this project:
https://geraldwiese.gitlab.io/gnuhealth-automatic-deployment/
