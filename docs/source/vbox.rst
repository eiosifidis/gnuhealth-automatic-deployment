Setup using VirtualBox
======================
If you want multiple virtual machines communicating with each other here is an example how to realize this:

* Install VirtualBox
* From the CLI create an internal network::

    $ VBoxManage dhcpserver add --netname intnet --ip 10.13.13.100 --netmask 255.255.255.0 --lowerip 10.13.13.101 --upperip 10.13.13.254 --enable

* Create up to 4 virtual machines
* Keep the default network adapter 1 "NAT", add and enable a second one "Internal Network" (Settings -> Network)
* Install an OS on the VMs, e.g. Ubuntu 20.04 LTS, Desktop version for desktop.yml + servers, choose "Install OpenSSH server" during server installations
* Fill /etc/hosts on every machine with domain/IP pairs (get them by `hostname` and `ip a` commands):

::

    domain_hmis 10.13.13.101
    domain_client 10.13.13.102

* If you face problems regarding the systems time install ntpdate because package managers don't like to install packages from the future::

    $ sudo apt install ntpdate
    $ sudo ntpdate de.pool.org
