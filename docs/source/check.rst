Initial check
=============

In the beginning of every deployment the tasks in `check.yml` are included.

First of all a SSH connection and a sudo password are needed. The easiest way is to simply set a username and password
in vault. For a more secure setup read the sections `Securing SSH & PWs` and `Encryption by Ansible-Vault`.

Then the operating system will be checked.
If it's neither part of the Suse nor of the Debian family the execution will be aborted.
If it's Suse or Debian but not openSUSE Leap 15.3, Ubuntu 20.04 LTS or Debian 11 you have to accept a warning.
This interactive warning can be disabled using the `ignore_os_warning` boolean on the bottom of every `vars` configuration file.

Until here the only other case is checking the backup path. If it contains a trailing "/" it will be removed (in order not to have two for later paths).

