Welcome to GNU Health - Automatic Deployment's documentation!
=============================================================

.. toctree::
   :maxdepth: 1
   :numbered:
   :caption: Contents:

   introduction
   requirements
   structure
   check
   gnuhealth
   thalamus
   orthanc
   common
   desktop
   examples
   vault
   firewall
   ssh
   vbox
   trouble
   license

