Project structure
=================

For every type of deployment there is one `.yml` file in the top level directory.
This is the playbook to be executed. Additionally there are subfolders with the corresponding name.
Inside those folders there are files needed for the deployment and two configuration files: `vault` and `vars`.
There you have to enter your specific needs and givens.

Apart from the deployment-specific subfolders the files `hosts` and `ansible.cfg` are of interest.
In `hosts` you have to set a domain or IP address for the corresponding deployment in case of remote access.
It's also possible to use numerical or alphabetical ranges like this:

::

    [desktop]
    client_domain_[a:z]
    192.168.101.1[00:99]

You can modify `ansible.cfg` if you wish to change your global ansible configuration options.

The folder `share` contains files used by multiple deployment scenarios.

::

    Repository
    ├── desktop/
    ├── gnuhealth/
    │   ├── vars
    │   ├── vault
    │   └── ...
    ├── orthanc/
    ├── share/
    ├── thalamus/
    ├── ansible.cfg
    ├── desktop.yml
    ├── gnuhealth.yml
    ├── hosts
    ├── orthanc.yml
    ├── thalamus.yml
    └── ...

For all servers the four main steps are controlled by the booleans `installation`, `cert_and_rproxy`, `security_features` & `cron_backups` in `server_subdir/vars`.
Substeps are also controlled by additional booleans. For example the WebDAV server will only be installed if installation and webdav booleans are true for gnuhealth.
In the next chapters the individual and common functionalities of the different deployments will be described in detail.
The following graphic gives already an overview:

.. image:: graphics/ansible_flow.png
