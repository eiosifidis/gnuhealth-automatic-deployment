Desktop - workstation
=====================

The desktop deployment mainly contains the GNU Health client.
The client itself will be installed using zypper for openSUSE and using pip inside a virtual environment using Ubuntu/Debian.
For this and the crypto plugin the version number set in vars will be used.
After downloading and before installing the crypto plugin the signature will be checked just like for the HMIS node on Ubuntu/Debian.
Besides the connection informations for the server and databases are already entered in the config.
Those informations are mostly taken from the HMIS node setup.
While `gh_profile_name` can be set as you wish you should not change `gh_tryton_user` unless you know what you are doing.
This configuration and the plugin will be placed in /etc/skel/ in order to show up for newly created operating system users.
If `copy_gh_config` is true it will also be copied to the home directory of the user set in vault.

For using the crypto plugin a GPG key is necessary.
If you want to create one set `gpg_create_key` true and set the GPG related values below.

The client does not have an option to ignore security risks for selfsigned certificates.
If you created one, set `trust_selfsigned_certs` to true in order to trust the certificate.

Finally you can also install MyGNUHealth but this is only tested successful on Ubuntu 21.04 and on openSUSE Leap 15.3
but there it takes a long time for updating the repositories.

