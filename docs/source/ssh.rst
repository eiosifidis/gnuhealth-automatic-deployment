Securing SSH & PWs
==================
If your server accepts SSH connections, the playbooks work directly if you specify the right domain/IP, username and
password. However you could consider securing SSH:

Enable host key checking
------------------------
* In vault change `vault_ssh_args` to '-o StrictHostKeyChecking=yes'

* Add the hosts fingerprint to known_hosts in ssh::

    $ ssh-keyscan -H domain >> ~/.ssh/known_hosts

* Login to the server and make sure the fingerprints match


Disable password-based ssh login and root login
-----------------------------------------------
* Generate a ssh key if necessary::

    $ ssh-keygen -t rsa

* Copy the public key to the server::

    $ ssh-copy-id -i ~/.ssh/key_rsa.pub user@server

  (check in ~/.ssh/authorized_keys at server)

* To disable password-based login and root login edit /etc/ssh/sshd_config as sudo:

::

    ChallengeResponseAuthentication no
    PasswordAuthentication no
    UsePAM no
    (PermitRootLogin no) root login seems not to work anyway by default on Ubuntu 20.04

* You might need to setup ssh-agent for ansible because it does not prompt for a password to unlock the private key (or try to make it prompt for the password)::

    $ ssh-agent bash
    $ ssh-add ~/.ssh/id_rsa (ssh private key)
    $ exit

* Try connecting::

    $ ansible gnuhealth -m ping -e "ansible_user='user'"

Handling the sudo password
--------------------------
If you use password stored in vault you could at least encrypt the vault file like it is
explained in the section `Encryption by Ansible-Vault`.

In order to avoid putting your sudo password in vault two different approaches for becoming sudo are tested:

* Set `use_ssh_pw_for_sudo` to false and run the playbook with the `-K` flag

* Enable passwordless sudo on the target system and set `use_ssh_pw_for_sudo` to false as well.
  For example `sudo visudo /etc/sudoers/` and change the line `%sudo   ALL=(ALL:ALL) ALL`
  to `%sudo  ALL=(ALL:ALL) NOPASSWD: ALL` on Ubuntu. For openSUSE use `wheel` instead of `sudo` and add your user to
  wheel: `sudo usermod -aG wheel username`.


