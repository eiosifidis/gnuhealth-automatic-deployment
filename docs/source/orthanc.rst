Orthanc
=======

The DICOM server Orthanc is simply installed as a system package. Additionally all other `orthanc-packages` set in vars
are installed - for example webviewer and database extensions.
The default selection contains all packages available for Debian, Ubuntu and openSUSE.

By default the internal HTTP server uses port 8042 and the HTTPS reverse proxy port 443.
Both can also be changed in vars modifying the variables `orthanc_port` and `https_port`.

After the installation the DICOM server will be disabled because the HTTP server can overtake all its functionalities with encryption.
In order to be able to access the HTTP server from outside remote access will be allowed and authentication enabled.
Besides query results are limitied to 100 entries to avoid the server becoming unresponsive.
Thereby some proposed suggestions concerning the security are already realized.
Here you find some suggestions regarding the storage and the firewall as well:

https://book.orthanc-server.com/faq/security.html

If you want to allow overwriting instances set `overwrite_instances` to true.

Finally three demo users are created with the following username:password pairs:

* admin:admin

* alice:bob

* bob:alice

For those demo users a LUA script will be copied in order to manage access permissions for the different HTTP methods.
The user admin has no restrictions, alice can only read / perform GET methods and bob can perform all methods except DELETE.
Of course those users have to be modified before making your system productive.
You find the configuration file and the LUA script in /etc/orthanc/.
The config files name is `orthanc.json` if using Debian/Ubuntu and `Configuration.json` if using openSUSE.

Other configuration options do not differ from the other servers and will be explained in the section `Common features for servers`.
