Common features for servers
===========================

Certificate and reverse proxy
-----------------------------
For all servers it's recommended to use a reverse proxy for enabling HTTPS.

First of all a certificate and a key is required for this.
If they already exist you have to set the correct paths using `my_cert` and `my_key` variables in vars.
If you run Ansible remotely and they are already on the server set `remote_source` to true.
The certificate and its chain are expected to be in one file. If you have your certificate "cert.pem" and its chain "chain.pem" combine it using::

    $ cp cert.pem cert-chain.pem
    $ cat chain.pem >> cert-chain.pem

Otherwise a selfsigned certificate can be created. Therefor set `create_selfsigned_cert` to true.
Furthermore `selfsigned_cert_params` and a `cert_admin_mail` is needed (all in vars).
The certificate will also be copied to the directory `fetch` for later use in the desktop deployment.

Depending on the boolean `apache` either Apache or Nginx will be used for the reverse proxy.
If choosing Apache additional SSL parameters will be used.
For Thalamus and Orthanc set `http_redirect` to true if you want an additional proxy forwarding HTTP to HTTPS.

Security features
-----------------
If you set `security_features` to true, some more features will be installed.

Fail2ban
~~~~~~~~
Fail2ban monitors malicious network behaviour in log files and bans IPs temporarily if their behaviour matches specific filters (putting IPs in so called "jails").
After installation it's only enabled for ssh. The scripts enable some filters for apache/nginx as well.
Sending mails was tested successfully in general.
However the scripts disable sending mails for stopping and starting jails because a simple restart of the service would send many mails otherwise.
For enabling this again, just remove the file /etc/fail2ban/action.d/sendmail-whois.local.
You can search for bans with the following command:

`sudo zgrep 'Ban' /var/log/fail2ban.log*`

https://www.fail2ban.org/

Automatic upgrades
~~~~~~~~~~~~~~~~~~
If `automatic_updates_reboot` is true as well, automatic upgrades and reboots are enabled.
On Debian/Ubuntu Unattended Upgrades realize this and for openSUSE Transactional Update + Reboot Manager is used.
For sure reboots should not be enabled in a system that has to be running 24/7.

https://wiki.debian.org/UnattendedUpgrades

https://kubic.opensuse.org/blog/2018-04-04-transactionalupdates/

Monitoring systemd services
~~~~~~~~~~~~~~~~~~~~~~~~~~~
In vars you can set systemd services that should be monitored.
Every hour cron runs a bash script that checks if they are still running, creates a log file if not and potentially sends an email as well.
To remove a service afterwards from the script, open it at /etc/cron.hourly/monitor_services and remove the corresponding block containing the services name.

E-mail notifications
~~~~~~~~~~~~~~~~~~~~
The option to send emails can be realized using sSMTP.
However this is not optimal since it requires to transfer your password into /etc/ssmtp/ssmtp.conf which can be read by users with sudo rights.
Thus you might want to find a different way to enable the "sendmail" command yourself - for example using postfix.
If you want to use sSMTP, create a new mail account for this and make sure that access from applications is allowed.
It's tested using web.de which requires to "allow IMAP/POP3" although only SMTP will be used.
To use this, set `use_mail` and `use_ssmtp` to true in vars, give the necessary informations below for your dummy mail
and set `security_admin_mail` which will receive notification mails.
Put the password into vault and encrypt it afterwards.

Backups
-------
Set `cron_backups` to true if you want to have automatic periodic backups using cron jobs.
A BASH script will then make one backup daily, weekly and monthly using depths for backup rotation as set in `vars`.
In case of local backups on an external disk the folder specified as `backup_directory` will be used.
For remote backups the path is given in the variable `remote_backup_directory` and besides connection informations are
needed. If you did not setup SSH yourself between the target server and the backup system you can let the scripts do it
but a password would be needed in vault.
For the GNU Health HMIS node and Orthanc the official backup instructions are followed in the script.
For Thalamus a database dump will be saved.

Follow these links for further informations for backup & restore:

https://en.wikibooks.org/wiki/GNU_Health/Control_Center

https://book.orthanc-server.com/users/backup.html
