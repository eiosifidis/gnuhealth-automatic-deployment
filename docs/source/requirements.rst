Requirements
============
- Install git and clone the repository::

    $ sudo apt install git
    $ git clone https://gitlab.com/geraldwiese/gnuhealth-automatic-deployment.git

- Install ansible::

    $ sudo apt install ansible

  and two collections as well::

    $ ansible-galaxy collection install community.crypto community.general

- In case of remote access:
    - Preferably setup a ssh key or otherwise install sshpass where you call the playbooks::

        $ sudo apt install sshpass

    - Make sure the remote machines are accessible via ssh::

        $ sudo apt install openssh-server
        $ sudo ufw allow ssh

- If you run the playbook on the target system you need neither sshpass nor openssh-server but have to set
  `--connection=local` when running the playbook, for example::

    $ ansible-playbook gnuhealth.yml --connection=local

- Hardware: If using virtual machines take 20GB storage and 2GB RAM to be sure. For testing purposes 1GB RAM and 15GB storage should be enough as well (for storage it's no difference if you allocate the storage dynamically anyway).
