Introduction
============

The Ansible playbooks in this project aim to automatically deploy the hospital information system GNU Health including the HMIS node, Thalamus, the DICOM server Orthanc and a desktop environment as a client using the GNU Health HMIS node.

**HMIS node**: Hospital management information system, core of the GNU Health system

**Thalamus** (GNU Health Federation): Creating a Network to converge multiple HMIS nodes and MyGNUHealth devices

**Orthanc**: The DICOM server is not directly part of the GNU Health system but its integration is provided

**Desktop**: Workstation with the GNU Health client to access the HMIS node

Supported operating systems are Ubuntu 20.04 LTS, openSUSE Leap 15.3 and Debian 11.

Using these playbooks you can create the following network:

.. image:: graphics/setup.png

