Encryption by Ansible-Vault
===========================
Don't store your passwords or private keys in plain text when non-root users can access them. Encrypting the password files::

    $ ansible-vault encrypt gnuhealth/vault

Afterwards the playbook is executed like this::

    $ ansibe-playbook gnuhealth.yml --ask-vault

If you access the target machine from remote but have the private key for the certificate on the system running ansible, encrypt it as well::

    $ ansible-vault encrypt /path/to/key.pem

Having two encrypted files - vault and the key - the playbook has to be run in the following way::

    $ ansible-playbook gnuhealth.yml --vault-id vault@prompt --vault-id key@prompt

For temporary unencrypted access decrypt the file like this::

    $ ansible-vault decrypt gnuhealth/vault
