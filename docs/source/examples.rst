Example usage
=============
Server and client in one
------------------------
GNU Health server and client on one system, running Ansible locally on the same system, generating a self signed certificate:

* Clone the repository and navigate inside its top level directory
* Set `vault_ssh_user` and `vault_ssh_password` in gnuhealth/vault
* Run the playbook gnuhealth.yml::

    $ ansible-playbook gnuhealth.yml --connection=local

* Set `vault_ssh_user` and `vault_ssh_password` in desktop/vault
* In desktop/vars set `trust_selfsigned_certs`: true
* Run the playbook desktop.yml::

    $ ansible-playbook desktop.yml --connection=local

* Run the client either using the desktop entry or from terminal::

    $ gnuhealth-client

* In the client the connection informations are already taken from the previous installation thus just hit `Connect`

* Enter the password (`vault_tryton_pw` in gnuhealth/vault, 789 if not changed)


Multiple systems
----------------
GNU Health server, Orthanc server and GNU Health client each on a different system, running Ansible remote, using existent certificate+key:

* Clone the repository and find out all domain names
* Set the corresponding domains or IPs under [gnuhealth], [orthanc] and [desktop] in the file hosts
* Set `vault_ssh_user` and `vault_ssh_password` in gnuhealth/vault
* In gnuhealth/vars set:

    * my_domain
    * create_selfsigned_cert: false
    * remote_source: true if cert+key are already on the target system
    * set paths my_cert and my_key
* Run: ansible-playbook gnuhealth.yml
* Repeat the previous steps for vars+vault in orthanc/ and run the playbook orthanc.yml
* Set user and password for desktop
* Run the playbook desktop.yml and run the client like described above

If you run Ansible from one of the three systems, install Ansible and sshpass on this one and openssh-server on the others.
Using ``--connection=local`` when calling the playbook will ignore IP/domain in hosts.
