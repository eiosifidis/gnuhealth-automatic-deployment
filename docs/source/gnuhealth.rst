GNU Health HMIS node
====================

The playbook `gnuhealth.yml` refers to the HMIS node which is the core of GNU Health. Basically the documentation is followed
from here for Debian/Ubuntu:

https://en.wikibooks.org/wiki/GNU_Health/Installation

And here for openSUSE:

https://en.opensuse.org/GNUHealth_on_openSUSE

Following the main installation instructions
--------------------------------------------

Using Debian/Ubuntu you will have an operating system user called `gnuhealth` having the password set for `vault_gh_password` in vault.
Using openSUSE a new user `tryton` will be created. In both cases this will be the user that has access to a newly created PostgreSQL database.
Its name is "health" by default and it is set by `my_db_name` in vars.

For openSUSE GNU Health can be installed as a system package.
For Debian/Ubuntu an archive will be downloaded and extracted if the signature is valid. Afterwards a shell script will be run.
Anyway `gnuhealth_version` from vars will be used. For Debian/Ubuntu `signing_key_id` and `signing_key_keyserver` are also necessary for signature checks.

The database will be initialized by Tryton. Meanwhile `tryton_admin_mail` from vars and `vault_tryton_pw` from vault are used.
This password will be used for connecting with the GNU Health client later on.

Finally the systemd service for GNU Health will be started and enabled thus it will always start after booting.

Additional steps
----------------

If you want a demo database as well, set `demo_db` to true. The `demo_db_version` 38 corresponds to the GNU Health version 3.8.
For this version you can choose one of the possible ones (38, 36, ...).
As `demo_db_name` you have to set the correct name which is `ghdemoxx` until here.

Additionally you can set `webdav` to true in order to have the WebDAV server installed for accessing calendars with third-party software.

By default the HMIS node and WebDAV have the internal ports 8000 & 8080 and the external ports 443 & 4443.
Except from the external port 443 all other ports can also be changed in vars.

Finally some Tryton modules will directly be activated in order to be able to use the Crypto plugin and Orthanc for example.
Adjust the `modules` in vars to fit your needs.
The federation module is outcommented because it might be used for Thalamus but it leads to needing an institution before creating patients in GNU Health.

Further configuration options will be explained in the section `Common features for servers`.
