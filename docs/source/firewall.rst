Firewall
========
The playbooks won't manage your firewall rules.
This might close a non-default ssh port or have no effect if running on virtual machines.
Make sure that incoming traffic is denied by default and that your internal server ports are closed but ssh and reverse proxies ports are open.
For example the GNU Health setup using the default ports requires 22, 443 and 4443 to be open if using WebDAV server and the internal ports 8000 and 8080 should be closed.

On Ubuntu you can activate the firewall, close/open ports and show the status like this::

    $ sudo ufw enable
    $ sudo ufw allow 443
    $ sudo ufw deny 8000
    $ sudo ufw status verbose

The equivalent for openSUSE Leap is::

    $ sudo systemctl start firewalld
    $ sudo firewall - cmd -- zone = public -- add - port =443/ tcp -- permanent
    $ sudo firewall - cmd -- zone = public -- remove - port =8000/ tcp -- permanent
    $ sudo firewall - cmd -- reload
    $ sudo firewall - cmd -- zone = public -- list - ports

For more informations about firewalls take a look here:

https://wiki.debian.org/Uncomplicated%20Firewall%20%28ufw%29

https://doc.opensuse.org/documentation/leap/security/html/book-security/cha-security-firewall.html
