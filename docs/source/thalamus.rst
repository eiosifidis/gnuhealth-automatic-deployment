Thalamus
========

For setting up Thalamus the installation instructions including uWSGI, venv & systemd service are followed from the wikibooks:

https://en.wikibooks.org/wiki/GNU_Health/Federation_Technical_Guide

By default the internal HTTP server uses port 443 (`http_port_in` in vars) and the default HTTPS port 443 is used for the
reverse proxy (`https_port_out`). If `populate_db` is true, some demo data will be added as well.
Similar to the HMIS node an operating system user `thalamus` will be created having the password set for
`vault_thlms_password` in vault.

Other configuration options do not differ from the other servers and will be explained in the section `Common features for servers`.
