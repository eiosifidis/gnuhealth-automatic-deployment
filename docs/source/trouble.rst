Troubleshooting
===============

* In case of error “WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!” e.g. after rebuilding a system you already connected to, run::

    $ ssh-keygen -R <IP/DN>

* If running gnuhealth.yml on Ubuntu/Debian was interrupted and fails afterwards because `trytond.conf` is not found:
  Remove the directory /home/gnuhealth/gnuhealth/ and run the playbook again
* If "get_url" tasks fail in the future, check the links manually and replace them if necessary
* For debugging uncomment lines in ansible.cfg and run the playbook with -vvv option